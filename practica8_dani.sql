create database persona;
use persona;
CREATE TABLE atributos (

    nif VARCHAR(9) UNIQUE,
    nombre VARCHAR(50) NOT NULL,
    apellido1 VARCHAR(50) NOT NULL,
    apellido2 VARCHAR(50)
);
insert into atributos(nif,nombre,apellido1,apellido2)
values('73108283T','daniel','gil','benedi');

select * from atributos;