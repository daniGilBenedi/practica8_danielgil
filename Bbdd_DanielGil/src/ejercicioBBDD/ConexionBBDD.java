package ejercicioBBDD;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
public class ConexionBBDD {

	private Connection conexion = null;
	PreparedStatement sentencia = null;

	public void conectar() {
		try {
			String servidor = "jdbc:mysql://localhost:3306/";
			String bbdd = "persona";
			String user = "root";
			String password = "";

			conexion = DriverManager.getConnection(servidor + bbdd, user, password);
			System.out.println("conectado");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void seleccionar() {
		String sentenciaSql = "SELECT * FROM atributos";
		try {
			sentencia = conexion.prepareStatement(sentenciaSql);

			ResultSet resultado = sentencia.executeQuery();
			// mostramos los datos
			while (resultado.next()) {
				System.out.println(resultado.getString(1) + ", " + resultado.getString(2) + ", "
						+ resultado.getString(3) + ", " + resultado.getString(4));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// nos aseguramos de cerrar los recursos abiertos
			if (sentencia != null) {
				try {
					sentencia.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void insertar(String nif, String nombre, String apellido1, String apellido2) {
		try {
			String sentenciaSql = "INSERT INTO atributos(nif, nombre, apellido1, apellido2) " + "values(?,?,?,?)";
			PreparedStatement sentencia;

			sentencia = conexion.prepareStatement(sentenciaSql);
			sentencia.setString(1, nif);
			sentencia.setString(2, nombre);
			sentencia.setString(3, apellido1);
			sentencia.setString(4, apellido2);
			sentencia.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// nos aseguramos de cerrar los recursos abiertos
			if (sentencia != null) {
				try {
					sentencia.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void actualizar(String nif, String nombre, String apellido1, String apellido2) {
		try {
			String sentenciaSql = "UPDATE atributos set " + "nombre=?, apellido1=?, apellido2=? WHERE nif=?";
			PreparedStatement sentencia;

			sentencia = conexion.prepareStatement(sentenciaSql);
			sentencia.setString(1, nombre);
			sentencia.setString(2, apellido1);
			sentencia.setString(3, apellido2);
			sentencia.setString(4, nif);
			sentencia.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// nos aseguramos de cerrar los recursos abiertos
			if (sentencia != null) {
				try {
					sentencia.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void eliminar(String nif) {
		try {
			String sentenciaSql = "DELETE FROM atributos WHERE nif=?";
			PreparedStatement sentencia;

			sentencia = conexion.prepareStatement(sentenciaSql);
			sentencia.setString(1, nif);
			sentencia.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// nos aseguramos de cerrar los recursos abiertos
			if (sentencia != null) {
				try {
					sentencia.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void desconectar() throws SQLException {
		sentencia.close();
	}
}
